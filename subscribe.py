from paho.mqtt.client import Client
import time
client = Client()


def on_connect(*args):
    print('MQTT: Connected to the broker', args[2])


def callback(*args):
    print(args[2].payload)


if __name__ == '__main__':
    client.on_connect = on_connect
    client.on_message = callback
    client.connect('192.168.1.117', 1883)
    client.loop_start()
    while True:
        client.subscribe('test')
        time.sleep(1)
