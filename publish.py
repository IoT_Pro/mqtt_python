from paho.mqtt.client import Client
import time
client = Client()


def on_connect(*args):
    print('MQTT: Connected to the broker', args[2])


if __name__ == '__main__':
    client.on_connect = on_connect
    client.connect('192.168.1.117', 1883)
    while True:
        client.publish('test', 'hello')
        time.sleep(1)
