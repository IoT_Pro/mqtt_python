# Instruction

This is MQTT communication on Raspberry Pi with Python.

Python version is 3.4.

---
## Install

You should install paho-mqtt in RPi using following command.
    
    sudo pip install paho-mqtt
    
---
## Usage
You shold copy this repository to your RPi.

Run publisher using following command.

    cd MQTT_Python
    python3 publish.py

Run subscriber using following command.

    cd MQTT_Python
    python3 subscribe.py
    
--- 
## Result
You can see the following result.

Publish:

![publish](image/publisher.PNG)

Subscribe:

![subscribe](image/subscriber.PNG)
